# Soluti Translation Bundle

A bundle that allows dumping messages right from the SymfonyProfiler

## Installation

#### A) Download and install

To install SolutiTranslationBundle run the following command

``` bash
$ php composer.phar require soluti/translation-bundle
```

#### B) Enable the bundle

Enable the required bundles in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles()
{
    ...
    if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
        ...
        $bundles[] = new Soluti\TranslationBundle\SolutiTranslationBundle();
        ...
    }
    ...
}
```
#### C) Enable the bundle routing

add the following to routing_dev.yml :
```yaml
soluti_translation:
    resource: "@SolutiTranslationBundle/Controller/"
    type:     annotation
```

## License

This package is available under the [MIT license](LICENSE).